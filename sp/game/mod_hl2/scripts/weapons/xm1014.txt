"Weapon_xm1014.back"
{
	"channel"		"CHAN_ITEM"
	"volume"		"1.0"
	"CompatibilityAttenuation"	"1.0"
	"pitch"			"95,105"

	"wave"			"weapons/xm1014/Back.wav"
}
"Weapon_xm1014.forward"
{
	"channel"		"CHAN_ITEM"
	"volume"		"1.0"
	"CompatibilityAttenuation"	"1.0"
	"pitch"			"95,105"

	"wave"			"weapons/xm1014/Forward.wav"
}

"Weapon_xm1014.insert"
{
	"channel"		"CHAN_ITEM"
	"volume"		"1.0"
	"CompatibilityAttenuation"	"1.0"
	"pitch"			"95,105"

	"wave"			"weapons/xm1014/insert.wav"
}
"Weapon_XM1014.Single"
{
	"channel"		"CHAN_WEAPON"
	"volume"		"1.0"
	"CompatibilityAttenuation"	"0.52"
	"pitch"		"PITCH_NORM"

	"wave"			")weapons/xm1014/xm1014-1.wav"
}
